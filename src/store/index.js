import { reactive } from "vue";
import User from "../class/User.ts"
import expressions from "../json/expressions.json"
import Conversition from "../class/Conversition.ts"
import E from "wangeditor";
import { useMessage } from "naive-ui";

const store = {
  state: reactive({
    sender: User,
    reciver: User,
    readyReciver: User,
    navId: 2,
    sessionList: [], //会话列表
    sessionSelectId: 0,
    allSessionList: [],
    allSessionSelectId: 0,
    socket: null,
    noCode: +new Date,
    navList: [
      { id: 1, name: "消息", icon: "icon-message" },
      { id: 2, name: "用户", icon: "icon-merbe" },
    ],
    conversitionList: [],
    sendInfo: "",
    expressions: expressions,
    expressionShow: false,
    contentScrollbar: null,
    chatEditor: null,
    editor: null,
    editorData: '',
    openMusic: false,
    tipMusic: null,
    message: useMessage(),
    theme: "default",
  }),
  // 设置对象数据
  setPropName(propName, value) {
    this.state[propName] = value;
  },
  // 设置会话窗口到达底部
  toBottom() {
    setTimeout(() => {
      store.state.contentScrollbar.scrollTo({ top: 99999 });
    }, 100)
  },
  // 修改信息已读状态
  changeReaded(id) {
    let userConversition = store.state.conversitionList.filter(
      (x) =>
        x.SendId == id &&
        x.ReciverId == store.state.sender.Id &&
        !x.ReadFlag
    );
    if (userConversition.length > 0) {
      userConversition.map((x) => {
        x.ReadFlag = true;
      });
    }
    let query = {
      SendId: id,
      ReciverId: store.state.sender.Id,
    };
    store.state.socket.emit("changeMsgRead", query);
  },
  // 初始化编辑器
  initEditor() {
    if (store.state.editor != null) {
      store.state.editor.destroy();
      store.state.editor = null;
    }
    store.state.editor = new E("#chatEditor");
    store.state.editor.config.showFullScreen = false;
    store.state.editor.config.focus = true;

    // 自定义菜单栏
    store.state.editor.config.menus = [];

    // change
    store.state.editor.config.onchange = (html) => {
      store.state.editorData = html;
    };

    // 上传最多1张
    store.state.editor.config.uploadImgMaxLength = 1;
    // 添加上传本地图片接口
    store.state.editor.config.customUploadImg = function (
      files,
      insert
    ) {
      insert(files);
    };
    // 聚焦操作
    setTimeout(() => {
      store.state.editor.create();
      store.state.editor.txt.html(store.state.editorData);
    }, 200)
  },
  // 声音提示
  playMusic() {
    if (store.state.tipMusic != null && store.state.openMusic) {
      store.state.tipMusic.currentTime = 0;
      store.state.tipMusic.play();
    }
  },
  // 提示消息
  tipMsg(msg) {
    store.state.message.info(msg, { duration: 1500 });
  },
  // 本地新增信息记录
  sendLocal(conversition) {
    store.state.conversitionList.push(conversition);
    store.toBottom();
  },
  // websocket发送消息
  sendInfo(conversition) {
    let data = {
      Conversition: conversition,
      ReciverId: store.state.reciver.Id,
      Sender: store.state.sender,
    };
    store.state.socket.emit("sendMsg", data);
  },
  // 初始化sender
  initSetInfo(data) {
    store.setPropName("sender", data?.myInfo);
    window.localStorage.setItem("token", data?.token);
    window.localStorage.setItem("sender", JSON.stringify(data?.myInfo));
  },
  // 注销
  logout() {
    window.localStorage.removeItem("token");
    window.localStorage.removeItem("sender");
    store.state.socket.disconnect();
    window.location.reload();
  },
  // 是否登陆
  isLogin() {
    return window.localStorage.getItem("token") ? true : false;
  }
}

export default store